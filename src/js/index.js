$(document).ready(function() {
    var windowScrolled;

    function tabState() {
        var hidden, visibilityChange;
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        }

        (function() {
            if (document[hidden]) {
                $('#video').get(0).pause();
            } else {
                $('#video').get(0).play();
            }
        }());

    }

    document.addEventListener('visibilitychange', tabState);


      (function toggleMenu () {


        var triggerOpen = $('.menuBtn');


        var triggerClose = $('.menu_main_box .close');


        var el = $('.menu_main_box');


    


        triggerOpen.on('click', function(e) {


         e.preventDefault();


         el.addClass('menu--active');


         el.animate({


           'top': '0'


         },800,'easeOutQuart');


       });


       triggerClose.on('click', function(e) {


         e.preventDefault();


         el.removeClass('menu--active');


         if($(window).width() < 767) {


           el.animate({


             'top': '-120%'


           },500,'easeInOutSine');


         }


         else {


           el.animate({


             'top': '-120%'


           },500,'easeInOutSine');


         }


       });


      }());

    function mainCarousel() {
        var owlSlider = $("#carousel-main");
        owlSlider.owlCarousel({
            center: true,
            dots: false,
            nav: true,
            navText: '',
            loop: true,
            smartSpeed: 800,
            items: 1
        });
    }
    mainCarousel();

    function carouselPush() {
        var el = $('#bd .mainSlide .owl-nav .owl-next');

        el.click();
    }

    function showFooter () {

        var triggerOpen = $('.show_feedback');
        var triggerClose = $('.close_feedback');
        var wrapper = $('#bd');
        var menu = $("#hd");

        triggerOpen.addClass('icon_dots--hidden');
        triggerClose.removeClass('icon_dots--hidden');

        if ($(window).width() > 1280) {
            $("#bd, #hd").css({
                'transform': 'translate3d(0,-' + 480 + 'px,0)'
            });
        } else if ($(window).width() < 1280 && $(window).width() > 767) {
            $("#bd, #hd").css({
                'transform': 'translate3d(0,-' + 500 + 'px,0)'
            });
            $('.mainSlide .mainSlide_text_wrap').css({
                'transform': 'translate3d(0,' + 98 + 'px,0)'
            });
        } else if ($(window).width() < 767) {
            $("#bd, #hd").css({
                'transform': 'translate3d(0,-' + 775 + 'px,0)'
            });
            $('.mainSlide .mainSlide_text_wrap').css({
                'transform': 'translate3d(0,' + 96 + 'px,0)'
            });
        }

    }

    function hideFooter () {

        var triggerOpen = $('.show_feedback');
        var triggerClose = $('.close_feedback');
        var wrapper = $('#bd');
        var menu = $("#hd");

        triggerClose.addClass('icon_dots--hidden');
        triggerOpen.removeClass('icon_dots--hidden');

        $(menu).css({
            'transform': 'translate3d(0,0,0)'
        });
        $('#bd').css({
            'transform': 'translate3d(0,0,0)'
        });
        $('.mainSlide .mainSlide_text_wrap').css({
            'transform': 'translate3d(0,0,0)'
        });

    }

    ( () => {

        $('.show_feedback').click(showFooter);
        $('.close_feedback').click(hideFooter);
        // $('body, html').scrolltrigger({
        //     scrollUp: hideFooter,
        //     scrollDown: showFooter
        // });

    })();


    function carousel() {
        var swiper = new Swiper('.swiper-container', {
            direction: 'vertical',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            loop: true,
            speed: 500
        });
        $('.slide-text-wrapper').dotdotdot({
            /*  The text to add as ellipsis. */
            fallbackToLetter: true,
            watch: true,
            height: 100
        });
    }

    function carouselShow() {
        $('.swiper-container').removeClass('carousel--hidden');
    }
    carousel();


    function numCount() {
        var one = $('.mainSlide_text .big .big__first__wrapper');
        var two = $('.mainSlide_text .big .big__second__wrapper');
        var three = $('.mainSlide_text .big .big__third__wrapper');

        var threee = $('.mainSlide_text .small .small_img img');
        var four = $('.mainSlide_text .small .small_text span');

        one.css({
            'transform': 'translateY(-50%)'
        });
        two.css({
            'transform': 'translateY(-75%)'
        });
        three.css({
            'transform': 'translateY(-75%)'
        });


        threee.css({
            'transform': 'translateX(0)'
        });
        four.css({
            'transform': 'translateX(14px)'
        });
    }

    function shutterOff() {
        var shutterWrapper = $('.shutter__wrapper');
        var ww = $(window).width();

        shutterWrapper.addClass('shutter__wrapper--off');

        function animation() {
            let tl = new TimelineMax({
                repeat: -1,
                onRepeat: () => {

                    //Refresh of video
                    document.querySelector('#video').currentTime = 0;
                    document.querySelector('#video').play();

                    //Mobile carousel push
                    setTimeout(() => {
                        carouselPush();
                    }, 1000);
                }
            });
            let $imgOne = $('.small_img img')[0];
            let $imgTwo = $('.small_img img')[1];
            let $imgThree = $('.small_img img')[2];
            let $imgFour = $('.small_img img')[3];
            let $imgFive = $('.small_img img')[4];

            let $rowOne = $('.row-1');
            let $rowTwo = $('.row-2');
            let $rowThree = $('.row-3');

            let $textLinesOne = $('.small_text_1 > span');
            let $textLinesTwo = $('.small_text_2 > span');
            let $textLinesThree = $('.small_text_3 > span');
            let $textLinesFour = $('.small_text_4 > span');
            let $textLinesFive = $('.small_text_5 > span');

            let $colOne = $('.big .col-text-1')[0];
            let $colTwo = $('.big .col-text-2')[0];
            let $colThree = $('.big .col-text-3')[0];
            let $colFour = $('.big .col-text-4')[0];

            let cycleDelay = 2;



            tl
                .addLabel('appearance', '+=1')
                // 0 — 1
                .to($imgOne, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, 'appearance')

                .staggerTo($textLinesOne, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, .1, 'appearance')

                .to($colOne, 1.1, {
                    y: '-24.7%',
                    ease: Power1.easeOut
                }, 'appearance')
                .to($colTwo, 1.3, {
                    y: '-24.88%',
                    ease: Power1.easeOut
                }, 'appearance')
                .to($colThree, 1.65, {
                    y: '-5.85%',
                    ease: Power1.easeOut
                }, 'appearance')
                .to($colFour, 1.65, {
                    y: '-24.9%',
                    ease: Power1.easeOut
                }, 'appearance')

                // 1 — 0(reverse)
                .addLabel('disappearance', '4.9')

                .to($imgOne, 1.3, {
                    x: '120%',
                    ease: Expo.easeIn,
                    delay: 0,
                    onComplete: carouselPush
                }, 'disappearance')

                .staggerTo($textLinesOne, 1.3, {
                    x: '-100%',
                    ease: Expo.easeIn,
                    delay: 0
                }, .1, 'disappearance')

                // 1 — 0(reverse)

                /// 1 — 2
                .addLabel('appearance1to2', '+=0.1')

                .to($imgTwo, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, 'appearance1to2')

                .staggerTo($textLinesTwo, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, .28, 'appearance1to2')

                .to($colOne, 1.1, {
                    y: '-49.7%',
                    ease: Power1.easeOut
                }, 'appearance1to2')
                .to($colTwo, 1.3, {
                    y: '-49.85%',
                    ease: Power1.easeOut
                }, 'appearance1to2')
                .to($colThree, 1.5, {
                    y: '-22.85%',
                    ease: Power1.easeOut
                }, 'appearance1to2')
                .to($colFour, 1.65, {
                    y: '-49.9%',
                    ease: Power1.easeOut
                }, 'appearance1to2')
                /// 1 — 2

                // 2 — 0(reverse)
                .addLabel('disappearance2', '+=3.68')

                .to($imgTwo, 1.3, {
                    x: '120%',
                    ease: Expo.easeIn,
                    onComplete: carouselPush
                }, 'disappearance2')

                .staggerTo($textLinesTwo, 1.3, {
                    x: '-100%',
                    ease: Expo.easeIn
                }, .1, 'disappearance2')

                // 2 — 0(reverse)

                // 2 — 3
                .addLabel('appearance2to3', '-=0.1')

                .to($imgThree, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, 'appearance2to3')

                .staggerTo($textLinesThree, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, .28, 'appearance2to3')

                .to($colOne, 1.1, {
                    y: '-74.85%%',
                    ease: Power1.easeOut
                }, 'appearance2to3')
                .to($colTwo, 1.3, {
                    y: '-74.85%',
                    ease: Power1.easeOut
                }, 'appearance2to3')
                .to($colThree, 1.65, {
                    y: '-74.85%',
                    ease: Power1.easeOut
                }, 'appearance2to3')
                .to($colFour, 1.65, {
                    y: '-74.9%',
                    ease: Power1.easeOut
                }, 'appearance2to3')
                // 2 — 3

                // 3 — 0(reverse)
                .addLabel('disappearance3', '+=4.2')

                .to($imgThree, 1.3, {
                    x: '120%',
                    ease: Expo.easeIn,
                    onComplete: carouselPush
                }, 'disappearance3')

                .staggerTo($textLinesThree, 1.3, {
                    x: '-100%',
                    ease: Expo.easeIn
                }, .1, 'disappearance3')

                // 3 — 0(reverse)

                // 3 — 4
                .addLabel('appearance3to4', '-=0.1')

                .to($imgFour, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, 'appearance3to4')

                .staggerTo($textLinesFour, 1.1, {
                    x: '0',
                    ease: Circ.easeOut
                }, .28, 'appearance3to4')

                .to($colOne, 1.1, {
                    y: '-99.7%',
                    ease: Power1.easeOut
                }, 'appearance3to4')
                .to($colTwo, 1.3, {
                    y: '-100.85%',
                    ease: Power1.easeOut
                }, 'appearance3to4')
                .to($colThree, 1.65, {
                    y: '-100.85%',
                    ease: Power1.easeOut
                }, 'appearance3to4')
                .to($colFour, 1.65, {
                    y: '-99.9%',
                    ease: Power1.easeOut
                }, 'appearance3to4')


                // 4 — 0(reverse)
                .addLabel('disappearance4', '+=3.8')

                .to($imgFour, 1.1, {
                    x: '120%',
                    ease: Expo.easeIn
                }, 'disappearance4')

                .staggerTo($textLinesFour, 1.1, {
                    x: '-100%',
                    ease: Expo.easeIn
                }, .1, 'disappearance4')

                .to($colOne, 2.5, {
                    y: '-122%',
                    ease: Power1.easeOut
                }, 'disappearance4')
                .to($colTwo, 1.3, {
                    y: '-111.7%',
                    ease: Power1.easeOut
                }, 'disappearance4')
                .to($colThree, 1.65, {
                    y: '-111.7%',
                    ease: Power1.easeOut
                }, 'disappearance4')
                .to($colFour, 1.65, {
                    y: '-111.7%',
                    ease: Power1.easeOut
                }, 'disappearance4')
        }

        animation();
        carouselShow();
    }

    function loadVideos() {
        var videoEl = document.querySelector('#video');
        var videoSection = $("#bd .mainSlide .video-wrapper");
        var carouselSection = $('#carousel-main');
        var video = document.querySelector('#video');

        function videoOnReady() {
            preloaderOut(0);
            setTimeout(function() {
                video.play();
            }, 1800);
            video.removeEventListener('canplaythrough', videoOnReady);
        }

        function videoOnPlay() {
            carouselSection.addClass('state--hidden');
            shutterOff();
            video.removeEventListener('playing', videoOnPlay);
        }

        function preloaderOut (delay) {

        	var interval;

        

        	setTimeout(() => {

        		$('.preloader__wrapper').addClass("preloader__wrapper--out");

        	},1200);

        

        	function removePreloader() {

        		$('.preloader__wrapper').remove();

        	}

        

        	interval = setInterval(function() {

        

        		if($('.preloader__wrapper').css('opacity') == '0') {

        			clearInterval(interval);

        			

        			setTimeout(removePreloader, delay);

        		}

        	},50);

        

        }

        if ($(window).width() > 1024) {

            video.addEventListener('canplaythrough', videoOnReady);

            video.addEventListener('playing', videoOnPlay);

        } else {
            videoSection.addClass('state--hidden');
            preloaderOut(200);
            setTimeout(function() {
                shutterOff();
            }, 1800);
        }

    }
    loadVideos();

    function formFeedback() {
        var form = $("#form"),
            inputs = $('#form #name, #form #email, #form #message'),
            footerClose = $('.icon_dots.close_feedback'),
            formData = form.serializeArray();


        $.ajax({
                url: form.data('action'),
                type: form.attr('method'),
                data: formData,
                beforeSend: function() {
                    var el = $('.submit__loader');
                    el.addClass("loader--active");
                }
            })
            .done((response) => {
                let errorCode = parseInt(response.code);


                $('.submit__loader').removeClass("loader--active");

                if (errorCode === 0) {

                    $('body').prepend($(`<div class="feedback__bar">${response.success}</div>`));
                    setTimeout(() => {
                        $('.feedback__bar').addClass('feedback__bar--active');
                    }, 300);

                    setTimeout(() => {
                        $('.feedback__bar').removeClass('feedback__bar--active');
                    }, 4000);

                    setTimeout(() => {
                        $('.feedback__bar').remove();
                    }, 4900);

                    footerClose.click();
                    inputs.val('');

                } else if (errorCode === 1) {

                    $('body').prepend($(`<div class="feedback__bar">${response.error}</div>`));

                    setTimeout(() => {
                        $('.feedback__bar').addClass('feedback__bar--active');
                    }, 150);

                    setTimeout(() => {
                        $('.feedback__bar').removeClass('feedback__bar--active');
                    }, 4000);

                    setTimeout(() => {
                        $('.feedback__bar').remove();
                    }, 4600);

                    $('.captcha img').click();
                    $('.input[name="captcha"]').focus();

                    $('.submit__loader').removeClass("loader--active");

                }
            });
    }


    $.validator.setDefaults({
        highlight: function(el) {
            $(el).next().addClass('error_text--active');
            $(el).addClass('error');
            if ($(el).hasClass('license')) {
                $('.license-label').addClass('checkbox--inactive');
            }
        },
        unhighlight: function(el) {
            $(el).next().removeClass('error_text--active');
            $(el).removeClass('error');
            if ($(el).hasClass('license')) {
                $('.license-label').removeClass('checkbox--inactive');
            }
        },
        errorPlacement: function(error, element) {
            element.next('.error_text').text(error.text());
            return true;
        }
    });

    $('#form').submit(function(e) {
        e.preventDefault();
    }).validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            capcha: {
                required: true
            },
            license: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Обязательное поле для ввода'
            },
            email: {
                required: 'Обязательное поле для ввода',
                email: 'Некорректный емейл'
            },
            capcha: {
                required: "Обязательное поле для ввода"
            }
        },
        submitHandler: function() {
            formFeedback();
            return false;
        }
    });


    function checkBox() {
        var el = $(this);
        var button = $('.submit');
        // var checkbox = $('form #license');
        if ($('#license').prop('checked')) {
            $('.license-label').addClass('checkbox--active');
        } else {
            $('.license-label').removeClass('checkbox--active');
        }
    }

    $('#license').on('change', checkBox);

    function textareaLabel() {
        var area = $('#message');
        var label = area.next();

        area.on('focus', function() {
            if ($(this).val().length <= 0) {
                label.css({
                    'opacity': '.3'
                });
            }
        });

        area.on('blur', function() {
            if ($(this).val().length <= 0) {
                label.css({
                    'opacity': '1'
                });
            } else {
                label.css({
                    'opacity': '0'
                });
            }
        });

        area.on('input', function() {
            if ($(this).val().length > 0) {
                label.css({
                    'opacity': '0'
                });
            } else {
                label.css({
                    'opacity': '.3'
                });
            }
        });
    }
    textareaLabel();


});


$.fn.extend({
    updateCaptcha: function(captchaKey, captchaHeight) {
        return this.each(function(index, object) {
            jQuery(object).prop('src', "/captcha.php?get_captcha=" + captchaKey + "&height=" + captchaHeight + "&anc=" + Math.floor(Math.random() * 100000));
        });
    }
});