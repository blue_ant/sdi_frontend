$(document).ready(function() {
      var windowScrolled;
      
//=include common/toggle-menu.js

//=include common/preloader-out.js
preloaderOut(200);

    (() => {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });

    })();

    $(window).on('resize', function() {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });
    });


  function toggleFooter () {
    var triggerOpen = $('.show_feedback');
    var triggerClose = $('.close_feedback');
    var el = $('.feedback_form_wrap');
//ПОПРОБОВАТЬ translate для более маленьких блоков

    triggerOpen.on('click', function() {
      $(this).addClass('icon_dots--hidden');
      triggerClose.removeClass('icon_dots--hidden');
      el.addClass('feedback_form_wrap--visible');
      if($(window).width() > 1280) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (262 + 190) + 'px)'
        })
      }
      else if ($(window).width() < 1280 && $(window).width() > 767) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (300 + 190) + 'px)'
        })
      }
      else if ($(window).width() < 767) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (240 + 190) + 'px)'
        })
      }
      return false;
    });

    triggerClose.on('click', function() {
      $(this).addClass('icon_dots--hidden');
      triggerOpen.removeClass('icon_dots--hidden');
      el.removeClass('feedback_form_wrap--visible');
      $('.main-translate').css({
        'transform': 'translateY(0px)'
      })
      return false;
    })

  }
  toggleFooter();

//=include common/show-out.js
showOut(1);

//=include common/anchor.js


})
