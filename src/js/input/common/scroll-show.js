  function onScroll() {

  var acnhorEl = $('#anchor-up');
  var anchorElBreak = $("#bd .parallax-el").height();
  var parallaxEl = $('.parallax-el');
  var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

  (() => {
      windowScrolled = $(window).scrollTop();
      if($(window).width() > 767) {
        parallaxEl.css({
        'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'
      });
      opacityEl.css({
        'opacity': 1 - windowScrolled/260
      });
      if($(window).scrollTop() > anchorElBreak) {
        acnhorEl.addClass('anchor-up--active');
      }
      else {
        acnhorEl.removeClass('anchor-up--active');
      }
    }
    else {
        parallaxEl.css({
        'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'
      });
      opacityEl.css({
        'opacity': 1 - windowScrolled/100
      });
      if($(window).scrollTop() > anchorElBreak) {
        acnhorEl.addClass('anchor-up--active');
      }
      else {
        acnhorEl.removeClass('anchor-up--active');
      }
  }
})();

    (() => {
      if($(window).width() < 767) {
        $('.show--transition').each(function() {
          showElem = $(this).offset().top;
          windowBottom = $(window).scrollTop() + $(window).height();

          // if((showElem - windowBottom) < -150) {
          //   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
          // }
        });
      }
      else if($(window).width() > 767) {
        $('.show--transition').each(function() {
          showElem = $(this).offset().top;
          windowBottom = $(window).scrollTop() + $(window).height();

          if((showElem - windowBottom) < -150) {
            $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
          }
        })
      }
    })();

    (() => {
      if($(window).scrollTop() > anchorElBreak) {
        acnhorEl.addClass('anchor-up--active');
      }
      else {
        acnhorEl.removeClass('anchor-up--active');
      }
    })();

  }