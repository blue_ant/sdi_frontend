  (function toggleMenu () {
    var triggerOpen = $('.menuBtn');
    var triggerClose = $('.menu_main_box .close');
    var el = $('.menu_main_box');

    triggerOpen.on('click', function(e) {
     e.preventDefault();
     el.addClass('menu--active');
     el.animate({
       'top': '0'
     },800,'easeOutQuart');
   });
   triggerClose.on('click', function(e) {
     e.preventDefault();
     el.removeClass('menu--active');
     if($(window).width() < 767) {
       el.animate({
         'top': '-120%'
       },500,'easeInOutSine');
     }
     else {
       el.animate({
         'top': '-120%'
       },500,'easeInOutSine');
     }
   });
  }());