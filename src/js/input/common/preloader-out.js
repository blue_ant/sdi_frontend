function preloaderOut (delay) {
	var interval;

	setTimeout(() => {
		$('.preloader__wrapper').addClass("preloader__wrapper--out");
	},1200);

	function removePreloader() {
		$('.preloader__wrapper').remove();
	}

	interval = setInterval(function() {

		if($('.preloader__wrapper').css('opacity') == '0') {
			clearInterval(interval);
			
			setTimeout(removePreloader, delay);
		}
	},50);

}