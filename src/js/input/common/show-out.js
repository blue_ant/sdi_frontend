  var delayList = [];

function showOut (delay) {
     $('.show--transition').each(function() {
     var showElem = $(this).offset().top;
     var windowBottom = $(window).scrollTop() + $(window).height();
     var windowTop = $(window).scrollTop();
     var parallaxEl = $('.parallax-el');
     var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

    setTimeout(  () => {
           if($(window).width() > 767) {
           if((showElem - windowBottom) < -100) {
             delayList.push($(this));
             $(this).css({
               'transition-delay': delayList.length/3 + delay + 's',
               '-webkit-transition-delay': delayList.length/3 + delay + 's',
             });

             parallaxEl.css({
             'transform': 'translate3d(0,' + $(window).scrollTop() / 3 + 'px,0)'
             });
             opacityEl.css({
               'opacity': 1 - $(window).scrollTop()/260
             });

             $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
           }
         }
         else {
           $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
         }
            //=include scroll-show.js
           $(window).scroll(onScroll);
       },300);
    });
}