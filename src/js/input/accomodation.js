$(document).ready(function() {
      var windowScrolled;

//=include common/toggle-menu.js

//=include common/preloader-out.js
preloaderOut(200);

  function carousel () {

    var windowWidth,arrowMargin;

    if($(window).width() > 1024) {
      windowWidth = $(window).width() / 3.56;
      setTimeout(function() {
        $('.owl-nav .owl-prev').css({
          'left': windowWidth - 32 + 'px'
        })
        $('.owl-nav .owl-next').css({
          'right': windowWidth - 32 + 'px'
        })
      },0)
    }
    else if ($(window).width() < 1024 && $(window).width() > 767) {
      windowWidth = $(window).width() / 8.3;
      setTimeout(function() {
        $('.owl-nav .owl-prev').css({
          'left': windowWidth - 32 + 'px'
        })
        $('.owl-nav .owl-next').css({
          'right': windowWidth - 32 + 'px'
        })
      },0)
    }
    else {
      windowWidth = 0;
    }


    var owlSlider = $('.owl-carousel');
    owlSlider.owlCarousel({
      center: true,
      dots: false,
      nav: true,
      navText: '',
      loop: true,
      stagePadding: windowWidth,
      smartSpeed: 500,
      responsive: {
        0: {
          items: 1,
        },
        767: {
          items: 1,
        },
        1023: {
          items: 1
        },
        1920: {
          items: 1
        }
      }
    })
  };

  carousel();

//=include common/show-out.js
showOut(1);

    //=include common/anchor.js

});
