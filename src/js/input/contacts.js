$(document).ready(function() {
      var windowScrolled;
      
//=include common/toggle-menu.js

//=include common/preloader-out.js
preloaderOut(200);

  function mapInit () {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: {
        lat: 55.763924,
        lng: 37.599705
      },
      disableDefaultUI: true,
      styles: [
          {elementType: 'geometry', stylers: [{color: '#313131'}]},
          {elementType: 'labels.text.stroke', stylers: [{color: '#292929'}]},
          {elementType: 'labels.text.fill', stylers: [{color: '#717171'}]},
          {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          },
          {
            featureType: "transit",
            stylers: [
              { visibility: "off" }
            ]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '717171'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#333333'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#2B2B2B'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#292929'}]
          }
        ]
    });

    google.maps.event.addListenerOnce(map, 'idle', function() {
      //=include common/show-out.js
      showOut(0.5);
    });

    var marker = new google.maps.Marker({
      position: {
        lat: 55.763924,
        lng: 37.599705
      },
      map: map
    });

  }

  mapInit();

});


