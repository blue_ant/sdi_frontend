$(document).ready(function() {
      var windowScrolled;

  //=include common/preloader-out.js
  preloaderOut(200);

  function addFadeClass () {
    var postText = $(".news__wrapper .news__post p");

    if(typeof postText !== 'undefined') {
      postText.addClass('show--transition show--hidden--bottom');
      postText.css('opacity', '1');
    }
  }

  addFadeClass();

//=include common/toggle-menu.js

//=include common/show-out.js
showOut(1);

//=include common/anchor.js


    (() => {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });

    })();

    $(window).on('resize', function() {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });
    });

    window.fbAsyncInit = function(){
      FB.init({
          appId: '1530369590354598', status: true, cookie: true, xfbml: true }); 
      };
      (function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
          if(d.getElementById(id)) {return;}
          js = d.createElement('script'); js.id = id; 
          js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
          ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));
      function postToFeed(title, desc, url, image){
      var obj = {method: 'feed',link: url, picture: 'http://www.url.com/images/'+image,name: title,description: desc};
      function callback(response){}
      FB.ui(obj, callback);
      }
      $('.share-facebook').click(function(){
        var elem = $(this);
        postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

        return false;
      });

      $(function() {
        $(document).click(function(e) {
          if(e.target.classList.contains('search_box') || e.target.classList.contains('search_input')) {
            $('.search_box').addClass('open');
          }
          else if(e.target.classList.contains('del_text')) {
            $('.search_input').val("");
          }
          else {
            $('.search_box').removeClass('open');
          }
        })
      });

      if($('.owl-carousel')) {

        function carousel () {

          var windowWidth,arrowMargin;

          if($(window).width() > 1024) {
            windowWidth = $(window).width() / 3.56;
            setTimeout(function() {
              $('.owl-nav .owl-prev').css({
                'left': -20 + 'px'
              })
              $('.owl-nav .owl-next').css({
                'right': -20 + 'px'
              })
            },0)
          }
          else if ($(window).width() < 1024 && $(window).width() > 767) {
            windowWidth = $(window).width() / 8.3;
            setTimeout(function() {
              $('.owl-nav .owl-prev').css({
                'left': -20 + 'px'
              })
              $('.owl-nav .owl-next').css({
                'right': -20 + 'px'
              })
            },0)
          }
          else {
            windowWidth = 0;
          }


          var owlSlider = $('.owl-carousel');
          $('.owl-carousel').owlCarousel({
            center: true,
            dots: false,
            nav: true,
            navText: '',
            loop: true,
            responsive: {
              0: {
                items: 1,
              },
              767: {
                items: 1,
              },
              1023: {
                items: 1
              },
              1920: {
                items: 1
              }
            }
          })
        };

        carousel();
      }

})
