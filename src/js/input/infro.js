$(document).ready(function() {
  var windowScrolled;
  
  //=include common/toggle-menu.js

  //=include common/preloader-out.js
preloaderOut(200);

  function cityOnMap() {

    $('.infrostr_map_list li a').on('click', function(e) {
      e.preventDefault();
      var self = $(this),
      cityNum = self.data('city'),
      spot = $(".infrostr_map_img .dot_" + cityNum),
      links = $('.infrostr_map_list a'),
      fade = $('.infrostr_map_img').find('.dot:not(".dot--transparent")');

      fade.addClass('dot--transparent');
      spot.removeClass('dot--transparent');
      links.removeClass('sel');
      self.addClass('sel');


    });
  }
  cityOnMap();

  function cardGallery() {
    $(".infrostr_img").flipping_gallery({
    direction: "forward", // This is will set the flipping direction when the gallery is clicked. Options available are "forward", or "backward". The default value is forward.
    selector: "> img", // This will let you change the default selector which by default, will look for <a> tag and generate the gallery from it. This option accepts normal CSS selectors.
    spacing: 10, // You can set the spacing between each photo in the gallery here. The number represents the pixels between each photos. The default value is 10.
    showMaximum: 15, // This will let you limit the number of photos that will be in the viewport. In case you have a gazillion photos, this is perfect to hide all those photos and limit only a few in the viewport.
    enableScroll: true, // Set this to false if you don't want the plugin to override your scrolling behavior. The default value is true.
    flipDirection: "right", // You can now set which direction the picture will flip to. Available options are "left", "right", "top", and "bottom". The default value is bottom.
    autoplay: false // You can set the gallery to autoplay by defining the interval here. This option accepts value in milliseconds. The default value is false.
  });
  }

  cardGallery();

//=include common/show-out.js
showOut(1);

//=include common/anchor.js

});
