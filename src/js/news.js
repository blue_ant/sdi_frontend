$(document).ready(function() {
      var windowScrolled;

  function preloaderOut (delay) {

  	var interval;

  

  	setTimeout(() => {

  		$('.preloader__wrapper').addClass("preloader__wrapper--out");

  	},1200);

  

  	function removePreloader() {

  		$('.preloader__wrapper').remove();

  	}

  

  	interval = setInterval(function() {

  

  		if($('.preloader__wrapper').css('opacity') == '0') {

  			clearInterval(interval);

  			

  			setTimeout(removePreloader, delay);

  		}

  	},50);

  

  }
  preloaderOut(200);

  function addFadeClass () {
    var postText = $(".news__wrapper .news__post p");

    if(typeof postText !== 'undefined') {
      postText.addClass('show--transition show--hidden--bottom');
      postText.css('opacity', '1');
    }
  }

  addFadeClass();

  (function toggleMenu () {

    var triggerOpen = $('.menuBtn');

    var triggerClose = $('.menu_main_box .close');

    var el = $('.menu_main_box');



    triggerOpen.on('click', function(e) {

     e.preventDefault();

     el.addClass('menu--active');

     el.animate({

       'top': '0'

     },800,'easeOutQuart');

   });

   triggerClose.on('click', function(e) {

     e.preventDefault();

     el.removeClass('menu--active');

     if($(window).width() < 767) {

       el.animate({

         'top': '-120%'

       },500,'easeInOutSine');

     }

     else {

       el.animate({

         'top': '-120%'

       },500,'easeInOutSine');

     }

   });

  }());

  var delayList = [];



function showOut (delay) {

     $('.show--transition').each(function() {

     var showElem = $(this).offset().top;

     var windowBottom = $(window).scrollTop() + $(window).height();

     var windowTop = $(window).scrollTop();

     var parallaxEl = $('.parallax-el');

     var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');



    setTimeout(  () => {

           if($(window).width() > 767) {

           if((showElem - windowBottom) < -100) {

             delayList.push($(this));

             $(this).css({

               'transition-delay': delayList.length/3 + delay + 's',

               '-webkit-transition-delay': delayList.length/3 + delay + 's',

             });



             parallaxEl.css({

             'transform': 'translate3d(0,' + $(window).scrollTop() / 3 + 'px,0)'

             });

             opacityEl.css({

               'opacity': 1 - $(window).scrollTop()/260

             });



             $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

           }

         }

         else {

           $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

         }

              function onScroll() {

            

              var acnhorEl = $('#anchor-up');

              var anchorElBreak = $("#bd .parallax-el").height();

              var parallaxEl = $('.parallax-el');

              var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

            

              (() => {

                  windowScrolled = $(window).scrollTop();

                  if($(window).width() > 767) {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/260

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                }

                else {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/100

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

              }

            })();

            

                (() => {

                  if($(window).width() < 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      // if((showElem - windowBottom) < -150) {

                      //   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      // }

                    });

                  }

                  else if($(window).width() > 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      if((showElem - windowBottom) < -150) {

                        $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      }

                    })

                  }

                })();

            

                (() => {

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                })();

            

              }

           $(window).scroll(onScroll);

       },300);

    });

}
showOut(1);



function anchorUp () {

  $('body, html').animate({

    'scrollTop' : 0

  },700);

}

$('#anchor-up').click(anchorUp);


    (() => {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });

    })();

    $(window).on('resize', function() {
      var blackText = $('.glow__wrapper');
      var measureText = $('.pale');

      blackText.each(function(item) {
        $(this).find('span').css({
          'width' : $(this).prev().width()
        });
      });
    });

    window.fbAsyncInit = function(){
      FB.init({
          appId: '1530369590354598', status: true, cookie: true, xfbml: true }); 
      };
      (function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
          if(d.getElementById(id)) {return;}
          js = d.createElement('script'); js.id = id; 
          js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
          ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));
      function postToFeed(title, desc, url, image){
      var obj = {method: 'feed',link: url, picture: 'http://www.url.com/images/'+image,name: title,description: desc};
      function callback(response){}
      FB.ui(obj, callback);
      }
      $('.share-facebook').click(function(){
        var elem = $(this);
        postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

        return false;
      });

      $(function() {
        $(document).click(function(e) {
          if(e.target.classList.contains('search_box') || e.target.classList.contains('search_input')) {
            $('.search_box').addClass('open');
          }
          else if(e.target.classList.contains('del_text')) {
            $('.search_input').val("");
          }
          else {
            $('.search_box').removeClass('open');
          }
        })
      });

      if($('.owl-carousel')) {

        function carousel () {

          var windowWidth,arrowMargin;

          if($(window).width() > 1024) {
            windowWidth = $(window).width() / 3.56;
            setTimeout(function() {
              $('.owl-nav .owl-prev').css({
                'left': -20 + 'px'
              })
              $('.owl-nav .owl-next').css({
                'right': -20 + 'px'
              })
            },0)
          }
          else if ($(window).width() < 1024 && $(window).width() > 767) {
            windowWidth = $(window).width() / 8.3;
            setTimeout(function() {
              $('.owl-nav .owl-prev').css({
                'left': -20 + 'px'
              })
              $('.owl-nav .owl-next').css({
                'right': -20 + 'px'
              })
            },0)
          }
          else {
            windowWidth = 0;
          }


          var owlSlider = $('.owl-carousel');
          $('.owl-carousel').owlCarousel({
            center: true,
            dots: false,
            nav: true,
            navText: '',
            loop: true,
            responsive: {
              0: {
                items: 1,
              },
              767: {
                items: 1,
              },
              1023: {
                items: 1
              },
              1920: {
                items: 1
              }
            }
          })
        };

        carousel();
      }

})
