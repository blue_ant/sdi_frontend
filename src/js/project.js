$(document).ready(function() {
      var windowScrolled;
      
  (function toggleMenu () {
      
    var triggerOpen = $('.menuBtn');
      
    var triggerClose = $('.menu_main_box .close');
      
    var el = $('.menu_main_box');
      

      
    triggerOpen.on('click', function(e) {
      
     e.preventDefault();
      
     el.addClass('menu--active');
      
     el.animate({
      
       'top': '0'
      
     },800,'easeOutQuart');
      
   });
      
   triggerClose.on('click', function(e) {
      
     e.preventDefault();
      
     el.removeClass('menu--active');
      
     if($(window).width() < 767) {
      
       el.animate({
      
         'top': '-120%'
      
       },500,'easeInOutSine');
      
     }
      
     else {
      
       el.animate({
      
         'top': '-120%'
      
       },500,'easeInOutSine');
      
     }
      
   });
      
  }());

function preloaderOut (delay) {

	var interval;



	setTimeout(() => {

		$('.preloader__wrapper').addClass("preloader__wrapper--out");

	},1200);



	function removePreloader() {

		$('.preloader__wrapper').remove();

	}



	interval = setInterval(function() {



		if($('.preloader__wrapper').css('opacity') == '0') {

			clearInterval(interval);

			

			setTimeout(removePreloader, delay);

		}

	},50);



}
preloaderOut(200);

  function toggleFooter () {
    var triggerOpen = $('.show_feedback');
    var triggerClose = $('.close_feedback');
    var el = $('.feedback_form_wrap');
//ПОПРОБОВАТЬ translate для более маленьких блоков

    triggerOpen.on('click', function() {
      $(this).addClass('icon_dots--hidden');
      triggerClose.removeClass('icon_dots--hidden');
      el.addClass('feedback_form_wrap--visible');
      if($(window).width() > 1280) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (262 + 190) + 'px)'
        })
      }
      else if ($(window).width() < 1280 && $(window).width() > 767) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (300 + 190) + 'px)'
        })
      }
      else if ($(window).width() < 767) {
        $('.main-translate').css({
          'transform': 'translateY(-' + (240 + 190) + 'px)'
        })
      }
      return false;
    })

    triggerClose.on('click', function() {
      $(this).addClass('icon_dots--hidden');
      triggerOpen.removeClass('icon_dots--hidden');
      el.removeClass('feedback_form_wrap--visible');
      $('.main-translate').css({
        'transform': 'translateY(0px)'
      })
      return false;
    })

  }
  toggleFooter();

  var delayList = [];



function showOut (delay) {

     $('.show--transition').each(function() {

     var showElem = $(this).offset().top;

     var windowBottom = $(window).scrollTop() + $(window).height();

     var windowTop = $(window).scrollTop();

     var parallaxEl = $('.parallax-el');

     var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');



    setTimeout(  () => {

           if($(window).width() > 767) {

           if((showElem - windowBottom) < -100) {

             delayList.push($(this));

             $(this).css({

               'transition-delay': delayList.length/3 + delay + 's',

               '-webkit-transition-delay': delayList.length/3 + delay + 's',

             });



             parallaxEl.css({

             'transform': 'translate3d(0,' + $(window).scrollTop() / 3 + 'px,0)'

             });

             opacityEl.css({

               'opacity': 1 - $(window).scrollTop()/260

             });



             $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

           }

         }

         else {

           $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

         }

              function onScroll() {

            

              var acnhorEl = $('#anchor-up');

              var anchorElBreak = $("#bd .parallax-el").height();

              var parallaxEl = $('.parallax-el');

              var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

            

              (() => {

                  windowScrolled = $(window).scrollTop();

                  if($(window).width() > 767) {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/260

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                }

                else {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/100

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

              }

            })();

            

                (() => {

                  if($(window).width() < 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      // if((showElem - windowBottom) < -150) {

                      //   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      // }

                    });

                  }

                  else if($(window).width() > 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      if((showElem - windowBottom) < -150) {

                        $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      }

                    })

                  }

                })();

            

                (() => {

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                })();

            

              }

           $(window).scroll(onScroll);

       },300);

    });

}
showOut(1);



function anchorUp () {

  $('body, html').animate({

    'scrollTop' : 0

  },700);

}

$('#anchor-up').click(anchorUp);

})
