$(document).ready(function() {
  var windowScrolled;
  
    (function toggleMenu () {
  
      var triggerOpen = $('.menuBtn');
  
      var triggerClose = $('.menu_main_box .close');
  
      var el = $('.menu_main_box');
  
  
  
      triggerOpen.on('click', function(e) {
  
       e.preventDefault();
  
       el.addClass('menu--active');
  
       el.animate({
  
         'top': '0'
  
       },800,'easeOutQuart');
  
     });
  
     triggerClose.on('click', function(e) {
  
       e.preventDefault();
  
       el.removeClass('menu--active');
  
       if($(window).width() < 767) {
  
         el.animate({
  
           'top': '-120%'
  
         },500,'easeInOutSine');
  
       }
  
       else {
  
         el.animate({
  
           'top': '-120%'
  
         },500,'easeInOutSine');
  
       }
  
     });
  
    }());

  function preloaderOut (delay) {

  	var interval;

  

  	setTimeout(() => {

  		$('.preloader__wrapper').addClass("preloader__wrapper--out");

  	},1200);

  

  	function removePreloader() {

  		$('.preloader__wrapper').remove();

  	}

  

  	interval = setInterval(function() {

  

  		if($('.preloader__wrapper').css('opacity') == '0') {

  			clearInterval(interval);

  			

  			setTimeout(removePreloader, delay);

  		}

  	},50);

  

  }
preloaderOut(200);

  function cityOnMap() {

    $('.infrostr_map_list li a').on('click', function(e) {
      e.preventDefault();
      var self = $(this),
      cityNum = self.data('city'),
      spot = $(".infrostr_map_img .dot_" + cityNum),
      links = $('.infrostr_map_list a'),
      fade = $('.infrostr_map_img').find('.dot:not(".dot--transparent")');

      fade.addClass('dot--transparent');
      spot.removeClass('dot--transparent');
      links.removeClass('sel');
      self.addClass('sel');


    });
  }
  cityOnMap();

  function cardGallery() {
    $(".infrostr_img").flipping_gallery({
    direction: "forward", // This is will set the flipping direction when the gallery is clicked. Options available are "forward", or "backward". The default value is forward.
    selector: "> img", // This will let you change the default selector which by default, will look for <a> tag and generate the gallery from it. This option accepts normal CSS selectors.
    spacing: 10, // You can set the spacing between each photo in the gallery here. The number represents the pixels between each photos. The default value is 10.
    showMaximum: 15, // This will let you limit the number of photos that will be in the viewport. In case you have a gazillion photos, this is perfect to hide all those photos and limit only a few in the viewport.
    enableScroll: true, // Set this to false if you don't want the plugin to override your scrolling behavior. The default value is true.
    flipDirection: "right", // You can now set which direction the picture will flip to. Available options are "left", "right", "top", and "bottom". The default value is bottom.
    autoplay: false // You can set the gallery to autoplay by defining the interval here. This option accepts value in milliseconds. The default value is false.
  });
  }

  cardGallery();

  var delayList = [];



function showOut (delay) {

     $('.show--transition').each(function() {

     var showElem = $(this).offset().top;

     var windowBottom = $(window).scrollTop() + $(window).height();

     var windowTop = $(window).scrollTop();

     var parallaxEl = $('.parallax-el');

     var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');



    setTimeout(  () => {

           if($(window).width() > 767) {

           if((showElem - windowBottom) < -100) {

             delayList.push($(this));

             $(this).css({

               'transition-delay': delayList.length/3 + delay + 's',

               '-webkit-transition-delay': delayList.length/3 + delay + 's',

             });



             parallaxEl.css({

             'transform': 'translate3d(0,' + $(window).scrollTop() / 3 + 'px,0)'

             });

             opacityEl.css({

               'opacity': 1 - $(window).scrollTop()/260

             });



             $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

           }

         }

         else {

           $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

         }

              function onScroll() {

            

              var acnhorEl = $('#anchor-up');

              var anchorElBreak = $("#bd .parallax-el").height();

              var parallaxEl = $('.parallax-el');

              var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

            

              (() => {

                  windowScrolled = $(window).scrollTop();

                  if($(window).width() > 767) {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/260

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                }

                else {

                    parallaxEl.css({

                    'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'

                  });

                  opacityEl.css({

                    'opacity': 1 - windowScrolled/100

                  });

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

              }

            })();

            

                (() => {

                  if($(window).width() < 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      // if((showElem - windowBottom) < -150) {

                      //   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      // }

                    });

                  }

                  else if($(window).width() > 767) {

                    $('.show--transition').each(function() {

                      showElem = $(this).offset().top;

                      windowBottom = $(window).scrollTop() + $(window).height();

            

                      if((showElem - windowBottom) < -150) {

                        $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');

                      }

                    })

                  }

                })();

            

                (() => {

                  if($(window).scrollTop() > anchorElBreak) {

                    acnhorEl.addClass('anchor-up--active');

                  }

                  else {

                    acnhorEl.removeClass('anchor-up--active');

                  }

                })();

            

              }

           $(window).scroll(onScroll);

       },300);

    });

}
showOut(1);



function anchorUp () {

  $('body, html').animate({

    'scrollTop' : 0

  },700);

}

$('#anchor-up').click(anchorUp);

});
