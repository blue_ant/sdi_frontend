$(document).ready(function() {
      var windowScrolled;

  (function toggleMenu () {

    var triggerOpen = $('.menuBtn');

    var triggerClose = $('.menu_main_box .close');

    var el = $('.menu_main_box');



    triggerOpen.on('click', function(e) {

     e.preventDefault();

     el.addClass('menu--active');

     el.animate({

       'top': '0'

     },800,'easeOutQuart');

   });

   triggerClose.on('click', function(e) {

     e.preventDefault();

     el.removeClass('menu--active');

     if($(window).width() < 767) {

       el.animate({

         'top': '-120%'

       },500,'easeInOutSine');

     }

     else {

       el.animate({

         'top': '-120%'

       },500,'easeInOutSine');

     }

   });

  }());

function preloaderOut (delay) {

	var interval;



	setTimeout(() => {

		$('.preloader__wrapper').addClass("preloader__wrapper--out");

	},1200);



	function removePreloader() {

		$('.preloader__wrapper').remove();

	}



	interval = setInterval(function() {



		if($('.preloader__wrapper').css('opacity') == '0') {

			clearInterval(interval);



			setTimeout(removePreloader, delay);

		}

	},50);



}
preloaderOut(200);

  function mapInit () {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: {
        lat: 55.735407,
        lng: 37.511283
      },
      disableDefaultUI: true,
      styles: [
          {elementType: 'geometry', stylers: [{color: '#313131'}]},
          {elementType: 'labels.text.stroke', stylers: [{color: '#292929'}]},
          {elementType: 'labels.text.fill', stylers: [{color: '#717171'}]},
          {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          },
          {
            featureType: "transit",
            stylers: [
              { visibility: "off" }
            ]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '717171'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#333333'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{color: '#292929'}]
          },
          {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#2B2B2B'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#717171'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#292929'}]
          }
        ]
    });

    google.maps.event.addListenerOnce(map, 'idle', function() {
        var delayList = [];

      function showOut (delay) {
           $('.show--transition').each(function() {
           var showElem = $(this).offset().top;
           var windowBottom = $(window).scrollTop() + $(window).height();
           var windowTop = $(window).scrollTop();
           var parallaxEl = $('.parallax-el');
           var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

          setTimeout(  () => {
                 if($(window).width() > 767) {
                 if((showElem - windowBottom) < -100) {
                   delayList.push($(this));
                   $(this).css({
                     'transition-delay': delayList.length/3 + delay + 's',
                     '-webkit-transition-delay': delayList.length/3 + delay + 's',
                   });

                   parallaxEl.css({
                   'transform': 'translate3d(0,' + $(window).scrollTop() / 3 + 'px,0)'
                   });
                   opacityEl.css({
                     'opacity': 1 - $(window).scrollTop()/260
                   });

                   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
                 }
               }
               else {
                 $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
               }
                    function onScroll() {

                    var acnhorEl = $('#anchor-up');
                    var anchorElBreak = $("#bd .parallax-el").height();
                    var parallaxEl = $('.parallax-el');
                    var opacityEl  = $('.parallax-el .head_page h1, .parallax-el .head_page ol');

                    (() => {
                        windowScrolled = $(window).scrollTop();
                        if($(window).width() > 767) {
                          parallaxEl.css({
                          'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'
                        });
                        opacityEl.css({
                          'opacity': 1 - windowScrolled/260
                        });
                        if($(window).scrollTop() > anchorElBreak) {
                          acnhorEl.addClass('anchor-up--active');
                        }
                        else {
                          acnhorEl.removeClass('anchor-up--active');
                        }
                      }
                      else {
                          parallaxEl.css({
                          'transform': 'translate3d(0,' + windowScrolled / 3 + 'px,0)'
                        });
                        opacityEl.css({
                          'opacity': 1 - windowScrolled/100
                        });
                        if($(window).scrollTop() > anchorElBreak) {
                          acnhorEl.addClass('anchor-up--active');
                        }
                        else {
                          acnhorEl.removeClass('anchor-up--active');
                        }
                    }
                  })();

                      (() => {
                        if($(window).width() < 767) {
                          $('.show--transition').each(function() {
                            showElem = $(this).offset().top;
                            windowBottom = $(window).scrollTop() + $(window).height();

                            // if((showElem - windowBottom) < -150) {
                            //   $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
                            // }
                          });
                        }
                        else if($(window).width() > 767) {
                          $('.show--transition').each(function() {
                            showElem = $(this).offset().top;
                            windowBottom = $(window).scrollTop() + $(window).height();

                            if((showElem - windowBottom) < -150) {
                              $(this).removeClass('show--hidden show--hidden--left show--hidden--right show--hidden--bottom');
                            }
                          })
                        }
                      })();

                      (() => {
                        if($(window).scrollTop() > anchorElBreak) {
                          acnhorEl.addClass('anchor-up--active');
                        }
                        else {
                          acnhorEl.removeClass('anchor-up--active');
                        }
                      })();

                    }
                 $(window).scroll(onScroll);
             },300);
          });
      }
      showOut(0.5);
    });

    var marker = new google.maps.Marker({
      position: {
        lat: 55.735407,
        lng: 37.511283
      },
      map: map
    });

  }

  mapInit();

});


